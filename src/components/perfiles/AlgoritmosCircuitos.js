import { Select } from "antd";
import useAlgoritmosCircuitos from "../../hooks/perfiles/useAlgoritmosCircuitos";

const { Option } = Select;

const AlgoritmosCircuitos = (props) => {
  console.log("AlgoritmosCircuitos props: ", props);
  const { AlgoritmosCircuitos, isLoadingAlgoritmosCircuitos } =
    useAlgoritmosCircuitos(
      props.eleccion,
      props.categoria,
      props.distrito,
      props.seccion,
      props.circuito,
      props.algoritmo,
      props.perfil
    );

  const handleChangeAlgoritmosCircuitos = (value) => {
    console.log(`AlgoritmosCircuitos handleChange ${value}`);
    props.onChange(value);
  };

  return (
    <Select
      style={{ width: 225, marginBottom: 40 }}
      placeholder="Algoritmos"
      value={props.algoritmo}
      loading={isLoadingAlgoritmosCircuitos}
      onChange={handleChangeAlgoritmosCircuitos}
      allowClear
    >
      <Option value="" />
      {AlgoritmosCircuitos.map((algoritmo) => (
        <Option key={algoritmo.cod_algoritmo} value={algoritmo.cod_algoritmo}>
          {algoritmo.nom_algoritmo}
        </Option>
      ))}
    </Select>
  );
};

export default AlgoritmosCircuitos;
