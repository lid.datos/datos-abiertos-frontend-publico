import logo from "../assets/logo.png";

const HeaderPrincipal = () => {
  return (
    <div className="container" style={{ padding: "10px 0" }}>
      <img
        src={logo}
        style={{ width: 240, display: "block" }}
        alt="La Izquierda Diario"
      />
    </div>
  );
};

export default HeaderPrincipal;
