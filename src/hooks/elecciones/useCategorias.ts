import { useEffect, useState } from "react";
import AppConfig from "../../AppConfig";
import { convertToQueryString } from "../utils";

const useCategorias = (eleccion: string, distrito: string) => {
  const [isLoading, setLoading] = useState(true);
  const [categorias, setCategorias] = useState<[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const myHeaders = new Headers();
        myHeaders.append("ngrok-skip-browser-warning", "1");
        const params = {
          eleccion: eleccion,
          distrito: distrito,
        };
        const res = await fetch(
          `${AppConfig.API_URL}/categorias?${convertToQueryString(params)}`,
          {
            headers: myHeaders,
          }
        );
        const json = await res.json();
        setCategorias(json);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, [eleccion, distrito]);

  return { isLoadingCategorias: isLoading, categorias };
};
export default useCategorias;
