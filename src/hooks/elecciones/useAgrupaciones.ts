import { useEffect, useState } from "react";
import AppConfig from "../../AppConfig";
import { convertToQueryString } from "../utils";

const useAgrupaciones = (
  eleccion: string,
  distrito: string,
  categoria: string
) => {
  const [isLoading, setLoading] = useState(true);
  const [agrupaciones, setAgrupaciones] = useState<[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const myHeaders = new Headers();
        myHeaders.append("ngrok-skip-browser-warning", "1");
        const params = {
          eleccion: eleccion,
          distrito: distrito,
          categoria: categoria,
        };
        const res = await fetch(
          `${AppConfig.API_URL}/agrupaciones?${convertToQueryString(params)}`,
          {
            headers: myHeaders,
          }
        );
        const json = await res.json();
        // @ts-ignore
        setAgrupaciones(Array.isArray(json) ? json : []);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, [eleccion, categoria, distrito]);

  return { isLoadingAgrupaciones: isLoading, agrupaciones };
};
export default useAgrupaciones;
