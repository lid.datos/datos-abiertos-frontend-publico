import { useEffect, useState } from "react";
import AppConfig from "../../AppConfig";
import { convertToQueryString } from "../utils";

const useMesas = (
  eleccion: string,
  distrito: string,
  seccion: string,
  circuito: string
) => {
  const [isLoading, setLoading] = useState(true);
  const [mesas, setMesas] = useState<[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const myHeaders = new Headers();
        myHeaders.append("ngrok-skip-browser-warning", "1");
        const params = {
          eleccion: eleccion,
          distrito: distrito,
          seccion: seccion,
          circuito: circuito,
        };
        const res = await fetch(
          `${AppConfig.API_URL}/mesas?${convertToQueryString(params)}`,
          {
            headers: myHeaders,
          }
        );
        const json = await res.json();
        // @ts-ignore
        setMesas(Array.isArray(json) ? json : []);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, [eleccion, distrito, seccion, circuito]);

  return { isLoadingMesas: isLoading, mesas };
};
export default useMesas;
