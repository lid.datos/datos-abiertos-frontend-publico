import { useEffect, useState } from "react";
import AppConfig from "../../AppConfig";
import { convertToQueryString } from "../utils";

const useAlgoritmosCircuitos = (
  eleccion: string,
  categoria: string,
  distrito: string,
  seccion: string,
  circuito: string,
  algoritmo: string,
  perfil: string
) => {
  const [isLoading, setLoading] = useState(true);
  const [AlgoritmosCircuitos, setAlgoritmosCircuitos] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const myHeaders = new Headers();
        myHeaders.append("ngrok-skip-browser-warning", "1");
        const params = {
          eleccion: eleccion,
          categoria: categoria,
          distrito: distrito,
          seccion: seccion,
          circuito: circuito,
          algoritmo: algoritmo,
          perfil: perfil,
        };
        const res = await fetch(
          `${
            AppConfig.API_URL
          }/perfiles/algoritmosCircuitosPerfiles?${convertToQueryString(
            params
          )}`,
          {
            headers: myHeaders,
          }
        );
        const json = await res.json();
        setAlgoritmosCircuitos(json);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, [eleccion, categoria, distrito, seccion, circuito, algoritmo, perfil]);

  return { isLoadingAlgoritmosCircuitos: isLoading, AlgoritmosCircuitos };
};
export default useAlgoritmosCircuitos;
