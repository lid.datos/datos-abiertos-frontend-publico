import { useEffect, useState } from "react";
import AppConfig from "../../AppConfig";
import { convertToQueryString } from "../utils";

const useEleccionesSecciones = (
  eleccion: string,
  categoria: string,
  distrito: string,
  seccion: string,
  algoritmo: string,
  perfil: string
) => {
  const [isLoading, setLoading] = useState(true);
  const [eleccionesSecciones, setEleccionesSecciones] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const myHeaders = new Headers();
        myHeaders.append("ngrok-skip-browser-warning", "1");
        const params = {
          eleccion: eleccion,
          categoria: categoria,
          distrito: distrito,
          seccion: seccion,
          algoritmo: algoritmo,
          perfil: perfil,
        };
        const res = await fetch(
          `${
            AppConfig.API_URL
          }/perfiles/eleccionesSeccionesPerfiles?${convertToQueryString(
            params
          )}`,
          {
            headers: myHeaders,
          }
        );
        const json = await res.json();
        setEleccionesSecciones(json);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, [eleccion, categoria, distrito, seccion, algoritmo, perfil]);

  return { isLoadingEleccionesSecciones: isLoading, eleccionesSecciones };
};
export default useEleccionesSecciones;
