import queryString from 'query-string';

function convertToQueryString(params: {}): string {
  return queryString.stringify(params, { skipNull: true, skipEmptyString: true });
}

export {
  convertToQueryString,
};
