# BUILD
FROM node:16-buster as builder

WORKDIR /usr/src/frontend

# Install app dependencies
# A wildcard is used to ensure both package.json AND
# package-lock.json are copied where available (npm@5+)
COPY package.json ./
RUN npm install

# Bundle app source
COPY ./src ./src
COPY ./public ./public
COPY ./tsconfig.json ./tsconfig.json

RUN npm run build

# HOST
FROM httpd:alpine
WORKDIR /usr/local/apache2/htdocs/
COPY --from=builder /usr/src/frontend/build .
COPY ./config/my-httpd.conf /usr/local/apache2/conf/httpd.conf