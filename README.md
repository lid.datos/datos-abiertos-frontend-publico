# Frontend para consultas a bases de datos

- Permite levantar contenedores en docker con **Frontend de consultas** electorales y otras localmente.

## Requisistos

- Docker y Docker Compose https://docs.docker.com/

## Qué hace

- Levanta una aplicacion React en http://localhost:9001 para hacer consultas a la base de datos electoral

## Instalando el proyecto y las bases de datos

1. Abrir una terminal y correr `docker-compose up -d`
2. Asegurarse de correr el proyecto de backend/api siguiendo las instrucciones de su LEEME https://gitlab.com/lid.datos/lid-bases-de-datos/-/tree/develop
3. Abrir en el navegador `http://localhost:9001/`

## Cómo realizar modificaciones

1. Realizar cambios en los archivos necesarios en `src`
2. Al recargar la pagina se verán reflejados los cambios

### Importante

Setear el archivo /src/AppConfig.ts con http://localhost:9000/api para correr localmente


### Licencia

Toda la infraestructura, así como las bases de datos están licenciados bajo la GNU General Public License, versión 3.
Para más detalles, consulte el archivo LICENCIA.txt presentes en los repositorios de fuentes y en el drive público con las bases de datos.
