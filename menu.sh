#!/bin/bash
# Bash Menu Script
while true
do
    echo "------------------------------------------------------------"
    echo "Ingresar opción: "
    echo "1) Levantar  contenedor: docker-compose up"
    echo "2) Detener   contenedor: docker-compose stop"
    echo "3) Reiniciar contenedor: docker-compose up --build"
    echo "4) Eliminar  contenedor: docker-compose down"
    echo "0) Salir"
    echo "------------------------------------------------------------"
    read opt
    case $opt in
        1) echo "- "$(date)" - Levantando contenedores (Ctrl+C para detener)" # Sin mostrar salida: docker-compose up -d 
           docker-compose up
           ;;
        2) # Detiene el contenedor que está corriendo sin borrarlo
           docker-compose stop
           ;;
        3) # Reconstruye imágenes antes de levantar contendores
           docker-compose up --build
           ;;
        4) # Borra el contenedor y su información asociada (redes, imágenes y volúmenes)
           docker-compose down
           ;;
        0) break
           ;;
        *) echo "opción inválida $opt";;
    esac
done
